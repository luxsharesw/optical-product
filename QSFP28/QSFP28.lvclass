﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str"></Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Optical Product.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Optical Product.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*.!!!*Q(C=\&gt;1R&lt;NN!%)8BZS"&amp;7A&amp;*(QOZQ41ZA+\!+UR+FWR&gt;J.!6*L!&lt;QZ6O%/A+=Q6WLH5&amp;_&gt;`FS"*3G%U-O$#JJ;3XO\/@3)J3X\Z)HX6=WGZ?X6L`0+A&gt;DX5YZ_X6/PPRZ8#+,PL0F;L=Z@Q:]5_2U`RZT$TVX&amp;_1:@`3JF?XD`[0`P@&gt;PXR\XS\`#&gt;ZWE[[[F*;UI$H._K]\*HG3*XG3*XG3"XG1"XG1"XG1/\G4/\G4/\G4'\G2'\G2'\G2^ZV=Z#)8/7=F74R:+#G;&amp;%A'1V&amp;S3$S**`%E(LYK]33?R*.Y%A^$F(A34_**0)G(;5I]C3@R**\%1[EOS&lt;[4YUE]F&amp;@A#4S"*`!%(J:5Y!E!Q7*"Y;!)$!7&gt;Q9@!%XA#$R]6?!*0Y!E]A9&gt;O":\!%XA#4_"B3D]LU46N*]&gt;$'4E?R_.Y()`DI&lt;1=D_.R0)\(]&lt;#=()`D=2$/AEZR#()G/1/=,Y\(]@!GR_.Y()`D=4RU^3PE`=QU4&gt;P*]2A?QW.Y$)`BI91-D_%R0)&lt;(]&amp;"7BM@Q'"\$9XB93I&lt;(]"A?!W)MSP)SCBE4D5&amp;'9(BY^&lt;P&amp;_F7+,L'_VV^TPF&amp;6.[$KRF,&gt;-+I&lt;187"62&gt;/&gt;5&amp;5*VJV!F5H2P7$64^%"61NL#KI'KA$RYGWJ_VI7^J)'WA&lt;WJKW;F0`]]$$Y;"JGL4@\\8&lt;\&lt;4&gt;&lt;D7/IY:BU';TU8K^VGKV?HF;@7?@N[P4=_G7TX@DX`O(XT_'BV]`\R_`8H`\]X1^N@T5`R:N@C[^BW?D0OFY]:DH($U$1?K?'1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.3</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#(D5F.31QU+!!.-6E.$4%*76Q!!(&gt;!!!!14!!!!)!!!(&lt;!!!!!J!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)!,:/&amp;53&amp;**LJ[[&gt;RA*7.!!!!!-!!!!%!!!!!!H.AI4(FU%3)HQ]J0;U-WJV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!=1V3_`OVT53AKC*A=/%S(Q%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#V8%V,=X9;%-9+]&amp;UO*1]S!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!,1!!!%RYH'-197"K9,D!!-3-1-T5Q01$S0Y!YD-)=%"FE&amp;D)'#++E!-!0?A6R!!!!!!!!%E!!!%9?*RD9-!%`Y%!3$%S-$$0!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=6%U'2)\:!#B]!NU=.CRG!Q"%_3?X!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!S!!!!==?*SFF=^L%U%5RW?XGTJ):0&gt;1N!@"28-))M&amp;C+5*\C'X3CP:(R0[!&amp;KNI$;W)"GG;5EV*C!LDOG#B&amp;`]"4R7-B^*Y3JN#[KX_!V+B3C""1AHK)2$@GZV.9E&amp;=='&amp;HPPNGXG=S8RYP\VS%T&amp;WYW%6;#7G2#$F"%O4/QZG\2QF]%_N2DJ$`@E+#&gt;`:1P#/&lt;#]0D5U]^Z&gt;_JENM)?,45%U_\('UT"DVN&lt;.#D--64K.:KN&gt;3W/W&gt;=PJ_`V).\$1LR@-"$]1.G"?@;N6H=:@.#1+/*R4.E8OH)4O&lt;SC5\*/NB'4PZZ`BD\?HVC0&amp;83D$(.'+*M:2^_N2&amp;P9ZE##,:3QD&amp;4BP&amp;!V72YV&lt;$K_`CY&amp;=&lt;MEBZ7P&lt;M([N9H%F;8&gt;V.\%E1XV7&gt;6W+W__)':OW;%MD2#T!G!#Y[@QD"#`UV[TUFLG,Z-79:T*CF\,4D^S,HCA(/0=W9Q@9#S*/@UA2#=!?2=&gt;=!ZT4EHU;+A:I&lt;UOFVG4'&gt;JS\%+DOF@YK9[XF4`.XF(2P+7D*[R9.E-55([S?)&amp;-U9&lt;2+@?,8"C2";767Q4";=8/=-//$\/]=L#MIJNIO$U/@U^XS4E\%HI8&lt;HB8&lt;2A,ONWN66ZN2'FY&gt;WQ!_^?=P*TS@+OR%O/E_,\P/JMHN.@WM.Z8:)QL'J8H?!YLLI+1=ZX)ASLWF5H/)[L&lt;IVTXG!(C*&lt;-@JUFL;K$WY',X,GEIK"`&amp;-;QM;#(OW/[OJK,TJI2G;6RT6S37=:;^MPB\FZ:8=V'B]V_W5IV*_2[.CT(:-TO&lt;'1`!H%I7VX@W=$?IA9XC]@5^6S2KOP:9AOIV,9W@80K"P1E0`1X#EV.W`SC'%EP/L'#)Y4=&lt;*3WZQ0UX((YFH99N$0GNLM?L@?T=14-?T47C:V0Q10Z(K-V\`@S0OBPM[::;VKUJAB/'\T$&lt;N0J8,X`PCV_`I"^O&lt;7J*`PB42!2&lt;(J^2#*YI!RD8/TN!3W,G.X&lt;5?MO3XO&lt;=A+O"B^T=+7K7$(5MFB0C#-R.A3R6U+0ALYF^"4IMG,JW[!V%:]$@&gt;ZF[1&gt;#VS`XF\FP*""MOH\^`_YX3D4=R1!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!O&lt;E!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!O&gt;(&amp;S^'Z!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!O&gt;(&amp;P\_`P]P2O1!!!!!!!!!!!!!!!!!!!!$``Q!!O&gt;(&amp;P\_`P\_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!0``!-P&amp;P\_`P\_`P\_`P\_`S^%!!!!!!!!!!!!!!!!!``]!R=7`P\_`P\_`P\_`P\``SQ!!!!!!!!!!!!!!!!$``Q$&amp;S]P&amp;P\_`P\_`P\`````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,R&lt;_`P\```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]82````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!S]P,S]P,S]P````````,SQ!!!!!!!!!!!!!!!!$``Q!!R=8,S]P,S``````,U=5!!!!!!!!!!!!!!!!!!0``!!!!!-8,S]P,```,S]5!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!$&amp;S]P,S\]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!R&lt;]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!D?!!!_5(C=T&gt;M0&lt;"08(1@QXTM@=-Y@=C9%S!3*YTLOHT7-1AM:!MI@&gt;Q7;MD1*,3!F9-=/:1VRBZ-GIR.BGYM;N%DLWC,5Q=;E+OL_N7JJB\IB&lt;6KD;-6&gt;`[Q&lt;7^N*+5D4VOZ0KQE'J4H@@P==HXVXTT\H:N?(R.-JX/`OX&lt;V0@P%X4Q#M;R%8=1EY*!%2,_."CQ2FI4A"'']390K0\QC)W]GH1/&lt;8%AH7#^P&amp;^\A%73R"23DO%Z&lt;Z2_"D0&amp;N_1X[9WU%_%3`AK&lt;0&amp;7LR9G126I@B#VV&lt;PJ/B^&lt;L&amp;X:&amp;&lt;KKC[I&amp;U&gt;*ANPBL&lt;]CD-9C?%/)X;#-LC;3!/*PY0G9JT81&amp;YZZF;][GY2;?EGH"+)`8HH!/XE^8B&amp;P`2N[37Y`.]1&gt;3FU3]*)XQ/H4J^.&amp;LG32DU[D'7OY);Q;Y@&lt;HK*HHDS_*?C&gt;PJD6FN!&lt;P-Z+[D\]_]&lt;AS&gt;[6)8VLND7-FFAV.0\&amp;3GCK4VN/SCR=P9BG/UW8X3T$@/\F2K"5OB.[&gt;68F0\!Q1),%X%T`(V&gt;ANS%%Z\(C@W_`;JCQ&amp;L:C(K^'YGIDN?.QOQ5WR/,=(_/E\/BLB"&amp;U,0L57;X!NS*@I7DD^)S+E&amp;_/%S7,Y_8(0EEW^!^(_]!&amp;XJ-@&gt;X2O)2NU0(NDX5+!`\!Y&amp;_A0':6LLDTN8+?^!O2E6!D8!/RLAY=R8(I'RM4&amp;]$4CG3^&gt;B[5,PJ&amp;LHIE]TL,\W50KV+X&gt;.P\`&lt;]@XZLX&lt;M6&gt;ZBYWIO":&gt;8Y*)0+&gt;^_?HQD0&gt;[=JLQ=+&lt;N%`51VF']L0/76+/S1DD)-/CLAU2QM6S7,UJ4R@(!MA=%=.=V9-ZR*'7M'M?:2=]J@^-&gt;&amp;L77F6G@ZW,&amp;DGDJ=C^7K::[1F'6Z3JZ3,'_1`_:Y'YZ4SUG@[(?T[J&gt;LRO_94_%L7L^&lt;M`J&gt;H(M&amp;X,."-2TTN,7X&lt;T'OQFX)&lt;967[E,AY6@19SKV23?V'C`^#M\&lt;6'ILPJX'H&gt;#Y#^*?(2KPU%8(@83=I/.&lt;;;`N'K_J[7K]&lt;C_]VXN:8I0Q$%2TW,N0\R80"XA:ADFK&gt;OC^9EU1;[,G8H=;P#KVZFZXK3O34;UU+P]^-1)(-^3WYK+%YL.B)^YK53&gt;0*4:$&amp;VW&amp;/;F6?*O*NBS8):A8WH(08%7N?^0^A&lt;[_=+`\6NWKP#`"(U*RXLH=/VGG;&lt;66Y)/VK=?78@3RLVW\BI_.9`)?M19YT(-RL`)6:V/MPX(JMJ\JSV:)]%?UP&gt;)\S&gt;'L,K-4&amp;`(/C[%#LX4W\&amp;G]%I[O?HR;4KQF#9&gt;\P$\ZPCPQ&amp;$=^J5%:2=`UGH5,(O8SRF/6Z7B12OWJTPLU&lt;-\D&lt;0!:(2GTK=0[G[T0*JD`&lt;)+;W:2,]#&gt;`P"9&lt;8&amp;6S58_&gt;@O-V/*NXXPU,TG&lt;AS#E[GVH+,:3L8&amp;W7P"U^`E,']&gt;+-Y[&lt;U]:7X-O[*M0^M_+F9DI_`Q&lt;T8P*02;^L+E\W'^RXB@'\Q.=A_4_J5R$S2QCR&gt;1MRV?MS`,4$G&amp;5&lt;-LR9(]TF&lt;99\&lt;#P.L*=$]/[O98W&gt;AHM8'P%`&amp;@%[?EC\J-4^19-T,D:B\CY/ZTV;9)\&lt;#`'!*-(`6+O:8'*BHMT&amp;XK:B0)?:T?MR\,'0GG:BP-7)/&amp;!&gt;TU&amp;;9OWW&amp;/61#T''LG(M9G/&gt;I-4OX:8TG&amp;^[&lt;'"$E@_#(;`Q\@:FWD/:K#C1$G!+&lt;-7NGJE$S_;QJ]-9]W`;=DC(XRHW"K#(@E*O.P\.)2KOI72)E4=QE/'D[WMB32B)5N,_Z7%0(/_H92M&gt;/.1G37RB*-+J*AG2&amp;Q:-AO:76"#/9N!ZH4X8ENCR*-*+D:C5D#5;QZL"J%C3L7%EQ9JI%3&lt;.J%ESUSH_67_#I*AG34D5*8M$TSO%"49MGO\/W[%C?=&amp;X4=(/%1&lt;+H9&amp;X[ONV\V&lt;Z)!NC*&lt;./F#@\-K,6.FS&lt;&gt;HXW8*MJ0"CN&gt;GI1:8&gt;K*88L#,=A@U_?'C1:B/(8I58="V)]CJ%VV@A:$YA7^]Y\#/T@G2,+^/-\PN:8T_WTF@%=*H/_U[HQ8QXH:T*T@K4I@2?&gt;H^-[X&amp;.[Z-5+3L=6R@J?NH,@9SPH&gt;*8#_T;LT,T/=F]`-_2L6?4=[(^5\8W@:/:`.O4&amp;&gt;ENO,YXS^L:RPM*8TD36QPMGK=T`$?56_TP.)H&gt;#@XDN`!1#4:Y^W\`'BL+FT6:\.8GA&lt;=L&gt;'"M-(D.NKA^FC:^!M&gt;M)1-X&lt;WG+@VLT.C:[6W!`)I(9`4=9S/T[=X))=:M4/IX9$]2O%X),`*CJV&gt;[6D(X%T]6J&lt;9W:7D*M;)H6XJK*JL!`)26OTM-N_!0');/_6K_10]"BX1&lt;E!_HWL@]N.YXOOQ3`O&lt;Q&gt;.:WX&gt;8HH)LWY:;=_U`PFC5VAUPW;FVQ]NW;NXQCR,]9P#8&amp;FMXH'7U\LET_IA#9[LRAWD];&lt;XR:QJLH,%N_;0C'0_RL9T`R&amp;&lt;'@VI#YT_T;PR:BP'KG2E`LBJP2?-(^=;@+KRRRG\F^YJD`)3ND*_UF@(PF]$Y$[Q;0]5Q,M\-_&amp;(6O!_.N_K.@^OS=:ZFH,'*_:XC'(`-6M;`;SPDDZ@!_".7D4`*-/YK7-QE([IR5`Y)CV[!M(:T]Z^:9_:+EW]!,B5T/\,&amp;40+P&lt;$%T9,K\_7^GT!S&lt;R`;0'$&amp;THH:X5V*'4K"D.2XLULO&lt;`W(%T)"W&gt;`.3Y8=X,\.C:C@'O,Y=/Z8`T2)T/X058'(%T%[M[40@X&lt;T+CJG&gt;ZLO&lt;HZD(T0-9-X]0`:K9S&gt;7F7D@MF;@A-&gt;CJ;&gt;W=/WPL\MTXYUF(TJD*.23F&gt;8-?/\6O\DI\N7\/_^GX&lt;K\2;OP_);.V6]`IYQF8L2J@C]&lt;X[IX8&amp;.;Y-7:S#YJD@+'ND#_SF@(;%BD`H&amp;8DIQTD]W&gt;G8&amp;#.6[0RN8LD:95V&lt;IS:8(FRD&amp;@9SHCFL9T0,9(R+KP'P]9Q8D/T(5V*D:E@I0&amp;KH8%C7T&lt;/MYQ&lt;9S9(R4&amp;/&lt;'7=MZ6R2QG-]V;.X]UQPO$`DZH'@V0LWLV_/&gt;=*YBUYMW%*;PS&lt;ZLUB&lt;M'X6#&amp;M%?Y14W)/)R)M%%Y+F]@0K``,?\R&amp;L9S^#&lt;U]ZXWK8FDU0^J*F5Y!!!!!!!1!!!%%!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!"]I!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!?18!)!!!!!!!1!)!$$`````!!%!!!!!!=A!!!!6!".!#A!.6(B1)%.I97ZO:7QA-1!41!I!$62Y5#"$;'&amp;O&lt;G6M)$)!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!T!".!#A!.6(B1)%.I97ZO:7QA.!!91&amp;!!"!!!!!%!!A!$#&amp;2Y)&amp;"P&gt;W6S!!!41!I!$6*Y5#"$;'&amp;O&lt;G6M)$%!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!S!".!#A!.5HB1)%.I97ZO:7QA-Q!41!I!$6*Y5#"$;'&amp;O&lt;G6M)$1!'%"1!!1!"1!'!!=!#!B3?#"1&lt;X&gt;F=A!!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-1!81!I!%62Y)%*J98-A1WBB&lt;GZF&lt;#!S!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$-!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA.!!71&amp;!!"!!+!!M!$!!."V2Y)%*J98-!&amp;5!'!!Z35V.*)%.I97ZO:7QA-1!!&amp;5!'!!Z35V.*)%.I97ZO:7QA-A!!&amp;5!'!!Z35V.*)%.I97ZO:7QA-Q!!&amp;5!'!!Z35V.*)%.I97ZO:7QA.!!!&amp;%"1!!1!$Q!1!"%!%A235V.*!!!?1&amp;!!"!!%!!E!$A!4$F&amp;42F!S/#ZM&gt;G.M98.T!!!"!"1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!&gt;2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!&amp;!!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!#!!!!!E!!!!+!!!!#Q!!!!Q!!!!.!!!!$A!!!!]!!!!1!!!!%1!!!")!!!!4!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$8&amp;N8\!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.=7V@M!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!"Z"=!A!!!!!!"!!A!-0````]!!1!!!!!"S!!!!"5!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!R!".!#A!.6(B1)%.I97ZO:7QA-A!41!I!$62Y5#"$;'&amp;O&lt;G6M)$-!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!U!"B!5!!%!!!!!1!#!!-)6(AA5'^X:8)!!".!#A!.5HB1)%.I97ZO:7QA-1!41!I!$6*Y5#"$;'&amp;O&lt;G6M)$)!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!T!".!#A!.5HB1)%.I97ZO:7QA.!!91&amp;!!"!!&amp;!!9!"Q!)#&amp;*Y)&amp;"P&gt;W6S!!!81!I!%62Y)%*J98-A1WBB&lt;GZF&lt;#!R!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$)!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-Q!81!I!%62Y)%*J98-A1WBB&lt;GZF&lt;#!U!":!5!!%!!I!#Q!-!!U(6(AA1GFB=Q!61!9!$F*45UEA1WBB&lt;GZF&lt;#!R!!!61!9!$F*45UEA1WBB&lt;GZF&lt;#!S!!!61!9!$F*45UEA1WBB&lt;GZF&lt;#!T!!!61!9!$F*45UEA1WBB&lt;GZF&lt;#!U!!!51&amp;!!"!!0!"!!%1!3"&amp;*45UE!!"Z!5!!%!!1!#1!/!"-/56.'5$)Y,GRW9WRB=X-!!!%!&amp;!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!;!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!)Y&amp;Q#!!!!!!"5!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!R!".!#A!.6(B1)%.I97ZO:7QA-A!41!I!$62Y5#"$;'&amp;O&lt;G6M)$-!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!U!"B!5!!%!!!!!1!#!!-)6(AA5'^X:8)!!".!#A!.5HB1)%.I97ZO:7QA-1!41!I!$6*Y5#"$;'&amp;O&lt;G6M)$)!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!T!".!#A!.5HB1)%.I97ZO:7QA.!!91&amp;!!"!!&amp;!!9!"Q!)#&amp;*Y)&amp;"P&gt;W6S!!!81!I!%62Y)%*J98-A1WBB&lt;GZF&lt;#!R!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$)!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-Q!81!I!%62Y)%*J98-A1WBB&lt;GZF&lt;#!U!":!5!!%!!I!#Q!-!!U(6(AA1GFB=Q!61!9!$F*45UEA1WBB&lt;GZF&lt;#!R!!!61!9!$F*45UEA1WBB&lt;GZF&lt;#!S!!!61!9!$F*45UEA1WBB&lt;GZF&lt;#!T!!!61!9!$F*45UEA1WBB&lt;GZF&lt;#!U!!!51&amp;!!"!!0!"!!%1!3"&amp;*45UE!!"Z!5!!%!!1!#1!/!"-/56.'5$)Y,GRW9WRB=X-!!!%!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!7!"9!!!!%!!!#*1!!!#A!!!!#!!!%!!!!!!5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"K1!!!YRYH*63S5\$-"3=YJ;GB3[UJ/RA^F5)WBYY2C!B=3+E@!!B3;&amp;32&amp;#&lt;1I`]*/)XY-S&amp;K2M7+&lt;GAE3W`]@/&lt;]&lt;-"\+.KZ&amp;'Y(JDS\.Z_?0"]?2SH[H'K%;?;G$6-J!'E-!;B81_E'4R\89R3L&lt;C'&amp;&gt;?QYBJ7AE9'Y]B#U[Q@D2GG4F(SN'0X`OAEUP6EOJ&amp;-.V&amp;4GHF-9"+&amp;&lt;,10X2B(U7KV,P\)*:$V*,+22$;";;651BF4K+3(O]!^0NZKLZ`MK^!P(]//9`P3\!:OXQE0`3?`=VO];JW&lt;^2-'DG`X?PEI&gt;%)@2[J?'DE556U[]`O^U/P+I#V6JHTM&gt;J\MU*/O(&gt;LM;1;]J-PX?Y@/1%1BW"DED#R%W\_$:LS)Q!J(B"?U5;;'Q"TGM3$[&lt;JM(][$=5(AJQP#Z)GIF1E5Z7V;1)BSY0'8SXSRC&amp;1@`=DU'4CF!QQW^JH(//B)&lt;V.!6.OH_'XK%/0/\M[5QOHE+WVQZKGZ+`?V^4+/'7;RB(4P9*&lt;&gt;,1-UD6$F+((P-7'&gt;5Y6O7S222Y0_:9,U=P7&lt;:EAR0#`KH_3`/&amp;['J!!!!!!!!D!!"!!)!!Q!%!!!!3!!0!!!!!!!0!/U!YQ!!!&amp;Y!$Q!!!!!!$Q$N!/-!!!"U!!]!!!!!!!]!\1$D!!!!CI!!A!#!!!!0!/U!YR6.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"631%Q5F.31QU+!!.-6E.$4%*76Q!!(&gt;!!!!14!!!!)!!!(&lt;!!!!!!!!!!!!!!!#!!!!!U!!!%#!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R$1V.5!!!!!!!!!:"-38:J!!!!!!!!!;2$4UZ1!!!!!!!!!&lt;B544AQ!!!!!!!!!=R%2E24!!!!!!!!!?"-372T!!!!!!!!!@2735.%!!!!!!!!!ABW:8*T!!!!"!!!!BR41V.3!!!!!!!!!I"(1V"3!!!!!!!!!J2*1U^/!!!!!!!!!KBJ9WQY!!!!!!!!!LR-37:Q!!!!!!!!!N"'5%BC!!!!!!!!!O2'5&amp;.&amp;!!!!!!!!!PB75%21!!!!!!!!!QR-37*E!!!!!!!!!S"#2%BC!!!!!!!!!T2#2&amp;.&amp;!!!!!!!!!UB73624!!!!!!!!!VR%6%B1!!!!!!!!!X".65F%!!!!!!!!!Y2)36.5!!!!!!!!!ZB71V21!!!!!!!!![R'6%&amp;#!!!!!!!!!]!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!!!!!!!!!!0````]!!!!!!!!!V!!!!!!!!!!!`````Q!!!!!!!!$I!!!!!!!!!!$`````!!!!!!!!!0!!!!!!!!!!!0````]!!!!!!!!"(!!!!!!!!!!!`````Q!!!!!!!!%E!!!!!!!!!!$`````!!!!!!!!!6A!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!!`````Q!!!!!!!!'Y!!!!!!!!!!4`````!!!!!!!!".Q!!!!!!!!!"`````]!!!!!!!!%\!!!!!!!!!!)`````Q!!!!!!!!4]!!!!!!!!!!H`````!!!!!!!!"1Q!!!!!!!!!#P````]!!!!!!!!&amp;(!!!!!!!!!!!`````Q!!!!!!!!5M!!!!!!!!!!$`````!!!!!!!!"51!!!!!!!!!!0````]!!!!!!!!&amp;7!!!!!!!!!!!`````Q!!!!!!!!8=!!!!!!!!!!$`````!!!!!!!!#?!!!!!!!!!!!0````]!!!!!!!!*]!!!!!!!!!!!`````Q!!!!!!!",5!!!!!!!!!!$`````!!!!!!!!%NQ!!!!!!!!!!0````]!!!!!!!!3Z!!!!!!!!!!!`````Q!!!!!!!",U!!!!!!!!!!$`````!!!!!!!!%VQ!!!!!!!!!!0````]!!!!!!!!4:!!!!!!!!!!!`````Q!!!!!!!"MU!!!!!!!!!!$`````!!!!!!!!'TQ!!!!!!!!!!0````]!!!!!!!!&lt;2!!!!!!!!!!!`````Q!!!!!!!"NQ!!!!!!!!!)$`````!!!!!!!!(3!!!!!!#F&amp;42F!S/#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!%!!%!!!!!!!!!!!!!!1!71&amp;!!!!Z25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!@``!!!!!1!!!!!!!1!!!!!"!":!5!!!$F&amp;42F!S/#ZM&gt;G.M98.T!!!"!!!!!!!"`````A!!!!!#&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9B&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!#!!01!I!#5.I97ZO:7QA-1!01!I!#5.I97ZO:7QA-A!01!I!#5.I97ZO:7QA-Q!01!I!#5.I97ZO:7QA.!!Y!0%!!!!!!!!!!2.$;'&amp;O&lt;G6M,5.M&gt;8.U:8)O9X2M!"R!5!!%!!!!!1!#!!-)6(AA5'^X:8)!!$A!]1!!!!!!!!!"%U.I97ZO:7QN1WRV=X2F=CZD&gt;'Q!(%"1!!1!!!!"!!)!!QB3?#"1&lt;X&gt;F=A!!.A$R!!!!!!!!!!%41WBB&lt;GZF&lt;#V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!"!!!!!%!!A!$"V2Y)%*J98-!:A$RVR&lt;4M1!!!!-64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T#F&amp;42F!S/#ZD&gt;'Q!,E"1!!-!"!!&amp;!!9&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!(!!!!!````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!1!!!!!!!Q!!!!!6!".!#A!.6(B1)%.I97ZO:7QA-1!41!I!$62Y5#"$;'&amp;O&lt;G6M)$)!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!T!".!#A!.6(B1)%.I97ZO:7QA.!!91&amp;!!"!!!!!%!!A!$#&amp;2Y)&amp;"P&gt;W6S!!!41!I!$6*Y5#"$;'&amp;O&lt;G6M)$%!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!S!".!#A!.5HB1)%.I97ZO:7QA-Q!41!I!$6*Y5#"$;'&amp;O&lt;G6M)$1!'%"1!!1!"1!'!!=!#!B3?#"1&lt;X&gt;F=A!!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-1!81!I!%62Y)%*J98-A1WBB&lt;GZF&lt;#!S!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$-!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA.!!71&amp;!!"!!+!!M!$!!."V2Y)%*J98-!&amp;5!'!!Z35V.*)%.I97ZO:7QA-1!!&amp;5!'!!Z35V.*)%.I97ZO:7QA-A!!&amp;5!'!!Z35V.*)%.I97ZO:7QA-Q!!&amp;5!'!!Z35V.*)%.I97ZO:7QA.!!!&amp;%"1!!1!$Q!1!"%!%A235V.*!!"I!0(8&amp;N8\!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-+56.'5$)Y,G.U&lt;!!Q1&amp;!!"!!%!!E!$A!4(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!&amp;!!!!"!!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!*!!!!#A!!!!M!!!!-!!!!$1!!!!\`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"I!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X.16%AQ!!!!,A!"!!1!!!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!</Property>
	<Item Name="QSFP28.ctl" Type="Class Private Data" URL="QSFP28.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="RSSI" Type="Folder">
			<Item Name="RSSI" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:RSSI:All</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RSSI</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RSSI.vi" Type="VI" URL="../Accessor/RSSI Property/Read RSSI.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'1!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"A!*1WBB&lt;GZF&lt;#!R!!^!"A!*1WBB&lt;GZF&lt;#!S!!^!"A!*1WBB&lt;GZF&lt;#!T!!^!"A!*1WBB&lt;GZF&lt;#!U!"2!5!!%!!5!"A!(!!A%5F.431!!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!E!#A!%!!1!"!!%!!M!"!!%!!Q#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RSSI.vi" Type="VI" URL="../Accessor/RSSI Property/Write RSSI.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'1!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$U!'!!F$;'&amp;O&lt;G6M)$%!$U!'!!F$;'&amp;O&lt;G6M)$)!$U!'!!F$;'&amp;O&lt;G6M)$-!$U!'!!F$;'&amp;O&lt;G6M)$1!&amp;%"1!!1!"Q!)!!E!#A235V.*!!![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!,!!Q#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RSSI Channel 1" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:RSSI:Channel 1</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RSSI Channel 1</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RSSI Channel 1.vi" Type="VI" URL="../Accessor/RSSI Channel 1 Property/Read RSSI Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"A!/5F.433"$;'&amp;O&lt;G6M)$%!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RSSI Channel 1.vi" Type="VI" URL="../Accessor/RSSI Channel 1 Property/Write RSSI Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;5!'!!Z35V.*)%.I97ZO:7QA-1!!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RSSI Channel 2" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:RSSI:Channel 2</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RSSI Channel 2</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RSSI Channel 2.vi" Type="VI" URL="../Accessor/RSSI Channel 2 Property/Read RSSI Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"A!/5F.433"$;'&amp;O&lt;G6M)$)!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RSSI Channel 2.vi" Type="VI" URL="../Accessor/RSSI Channel 2 Property/Write RSSI Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;5!'!!Z35V.*)%.I97ZO:7QA-A!!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RSSI Channel 3" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:RSSI:Channel 3</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RSSI Channel 3</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RSSI Channel 3.vi" Type="VI" URL="../Accessor/RSSI Channel 3 Property/Read RSSI Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"A!/5F.433"$;'&amp;O&lt;G6M)$-!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RSSI Channel 3.vi" Type="VI" URL="../Accessor/RSSI Channel 3 Property/Write RSSI Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;5!'!!Z35V.*)%.I97ZO:7QA-Q!!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RSSI Channel 4" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:RSSI:Channel 4</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RSSI Channel 4</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RSSI Channel 4.vi" Type="VI" URL="../Accessor/RSSI Channel 4 Property/Read RSSI Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"A!/5F.433"$;'&amp;O&lt;G6M)$1!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RSSI Channel 4.vi" Type="VI" URL="../Accessor/RSSI Channel 4 Property/Write RSSI Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;5!'!!Z35V.*)%.I97ZO:7QA.!!!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Rx Power" Type="Folder">
			<Item Name="Rx Power" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Rx Power:All</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Rx Power</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Rx Power.vi" Type="VI" URL="../Accessor/Rx Power Property/Read Rx Power.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1WBB&lt;GZF&lt;#!R!!^!#A!*1WBB&lt;GZF&lt;#!S!!^!#A!*1WBB&lt;GZF&lt;#!T!!^!#A!*1WBB&lt;GZF&lt;#!U!":!5!!%!!5!"A!(!!A(1WBB&lt;GZF&lt;!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
				</Item>
				<Item Name="Write Rx Power.vi" Type="VI" URL="../Accessor/Rx Power Property/Write Rx Power.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$U!+!!F$;'&amp;O&lt;G6M)$%!$U!+!!F$;'&amp;O&lt;G6M)$)!$U!+!!F$;'&amp;O&lt;G6M)$-!$U!+!!F$;'&amp;O&lt;G6M)$1!&amp;E"1!!1!"Q!)!!E!#A&gt;$;'&amp;O&lt;G6M!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!M!$!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
				</Item>
			</Item>
			<Item Name="RxP Channel 1" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Rx Power:Channel 1</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RxP Channel 1</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RxP Channel 1.vi" Type="VI" URL="../Accessor/RxP Channel 1 Property/Read RxP Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.5HB1)%.I97ZO:7QA-1!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RxP Channel 1.vi" Type="VI" URL="../Accessor/RxP Channel 1 Property/Write RxP Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!R!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RxP Channel 2" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Rx Power:Channel 2</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RxP Channel 2</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RxP Channel 2.vi" Type="VI" URL="../Accessor/RxP Channel 2 Property/Read RxP Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.5HB1)%.I97ZO:7QA-A!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RxP Channel 2.vi" Type="VI" URL="../Accessor/RxP Channel 2 Property/Write RxP Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!S!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RxP Channel 3" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Rx Power:Channel 3</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RxP Channel 3</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RxP Channel 3.vi" Type="VI" URL="../Accessor/RxP Channel 3 Property/Read RxP Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.5HB1)%.I97ZO:7QA-Q!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RxP Channel 3.vi" Type="VI" URL="../Accessor/RxP Channel 3 Property/Write RxP Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!T!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="RxP Channel 4" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Rx Power:Channel 4</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RxP Channel 4</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read RxP Channel 4.vi" Type="VI" URL="../Accessor/RxP Channel 4 Property/Read RxP Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.5HB1)%.I97ZO:7QA.!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write RxP Channel 4.vi" Type="VI" URL="../Accessor/RxP Channel 4 Property/Write RxP Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V3?&amp;!A1WBB&lt;GZF&lt;#!U!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Tx Bias" Type="Folder">
			<Item Name="Tx Bias" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Bias:All</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Bias</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tx Bias.vi" Type="VI" URL="../Accessor/Tx Bias Property/Read Tx Bias.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1WBB&lt;GZF&lt;#!R!!^!#A!*1WBB&lt;GZF&lt;#!S!!^!#A!*1WBB&lt;GZF&lt;#!T!!^!#A!*1WBB&lt;GZF&lt;#!U!":!5!!%!!5!"A!(!!A(1WBB&lt;GZF&lt;!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
				</Item>
				<Item Name="Write Tx Bias.vi" Type="VI" URL="../Accessor/Tx Bias Property/Write Tx Bias.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$U!+!!F$;'&amp;O&lt;G6M)$%!$U!+!!F$;'&amp;O&lt;G6M)$)!$U!+!!F$;'&amp;O&lt;G6M)$-!$U!+!!F$;'&amp;O&lt;G6M)$1!&amp;E"1!!1!"Q!)!!E!#A&gt;$;'&amp;O&lt;G6M!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!M!$!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
				</Item>
			</Item>
			<Item Name="Tx Bias Channel 1" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Bias:Channel 1</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Bias Channel 1</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tx Bias Channel 1.vi" Type="VI" URL="../Accessor/Tx Bias Channel 1 Property/Read Tx Bias Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$%!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write Tx Bias Channel 1.vi" Type="VI" URL="../Accessor/Tx Bias Channel 1 Property/Write Tx Bias Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="Tx Bias Channel 2" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Bias:Channel 2</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Bias Channel 2</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tx Bias Channel 2.vi" Type="VI" URL="../Accessor/Tx Bias Channel 2 Property/Read Tx Bias Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$)!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write Tx Bias Channel 2.vi" Type="VI" URL="../Accessor/Tx Bias Channel 2 Property/Write Tx Bias Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-A![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="Tx Bias Channel 3" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Bias:Channel 3</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Bias Channel 3</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tx Bias Channel 3.vi" Type="VI" URL="../Accessor/Tx Bias Channel 3 Property/Read Tx Bias Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$-!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write Tx Bias Channel 3.vi" Type="VI" URL="../Accessor/Tx Bias Channel 3 Property/Write Tx Bias Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA-Q![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="Tx Bias Channel 4" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Bias:Channel 4</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Bias Channel 4</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tx Bias Channel 4.vi" Type="VI" URL="../Accessor/Tx Bias Channel 4 Property/Read Tx Bias Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!#A!26(AA1GFB=S"$;'&amp;O&lt;G6M)$1!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write Tx Bias Channel 4.vi" Type="VI" URL="../Accessor/Tx Bias Channel 4 Property/Write Tx Bias Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;U!+!"&amp;5?#"#;7&amp;T)%.I97ZO:7QA.!![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Tx Power" Type="Folder">
			<Item Name="Tx Power" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Power:All</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Power</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tx Power.vi" Type="VI" URL="../Accessor/Tx Power Property/Read Tx Power.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1WBB&lt;GZF&lt;#!R!!^!#A!*1WBB&lt;GZF&lt;#!S!!^!#A!*1WBB&lt;GZF&lt;#!T!!^!#A!*1WBB&lt;GZF&lt;#!U!":!5!!%!!5!"A!(!!A(1WBB&lt;GZF&lt;!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
				</Item>
				<Item Name="Write Tx Power.vi" Type="VI" URL="../Accessor/Tx Power Property/Write Tx Power.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'3!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$U!+!!F$;'&amp;O&lt;G6M)$%!$U!+!!F$;'&amp;O&lt;G6M)$)!$U!+!!F$;'&amp;O&lt;G6M)$-!$U!+!!F$;'&amp;O&lt;G6M)$1!&amp;E"1!!1!"Q!)!!E!#A&gt;$;'&amp;O&lt;G6M!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!M!$!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
				</Item>
			</Item>
			<Item Name="TxP Channel 1" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Power:Channel 1</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TxP Channel 1</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read TxP Channel 1.vi" Type="VI" URL="../Accessor/TxP Channel 1 Property/Read TxP Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.6(B1)%.I97ZO:7QA-1!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write TxP Channel 1.vi" Type="VI" URL="../Accessor/TxP Channel 1 Property/Write TxP Channel 1.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!R!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="TxP Channel 2" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Power:Channel 2</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TxP Channel 2</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read TxP Channel 2.vi" Type="VI" URL="../Accessor/TxP Channel 2 Property/Read TxP Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.6(B1)%.I97ZO:7QA-A!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write TxP Channel 2.vi" Type="VI" URL="../Accessor/TxP Channel 2 Property/Write TxP Channel 2.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!S!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="TxP Channel 3" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Power:Channel 3</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TxP Channel 3</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read TxP Channel 3.vi" Type="VI" URL="../Accessor/TxP Channel 3 Property/Read TxP Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.6(B1)%.I97ZO:7QA-Q!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write TxP Channel 3.vi" Type="VI" URL="../Accessor/TxP Channel 3 Property/Write TxP Channel 3.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!T!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
			<Item Name="TxP Channel 4" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Power:Channel 4</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TxP Channel 4</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read TxP Channel 4.vi" Type="VI" URL="../Accessor/TxP Channel 4 Property/Read TxP Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!.6(B1)%.I97ZO:7QA.!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				</Item>
				<Item Name="Write TxP Channel 4.vi" Type="VI" URL="../Accessor/TxP Channel 4 Property/Write TxP Channel 4.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V5?&amp;!A1WBB&lt;GZF&lt;#!U!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
				</Item>
			</Item>
		</Item>
	</Item>
	<Item Name="Control" Type="Folder">
		<Item Name="Channel Boolean-Cluster.ctl" Type="VI" URL="../Control/Channel Boolean-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#G!!!!"1!/1#%*1WBB&lt;GZF&lt;#!R!!Z!)1F$;'&amp;O&lt;G6M)$)!$E!B#5.I97ZO:7QA-Q!/1#%*1WBB&lt;GZF&lt;#!U!'9!]1!!!!!!!!!$&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=RB$;'&amp;O&lt;G6M)%:M97=N1WRV=X2F=CZD&gt;'Q!)%"1!!1!!!!"!!)!!QR$;'&amp;O&lt;G6M)%:M97=!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Channel Value-Cluster.ctl" Type="VI" URL="../Control/Channel Value-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#@!!!!"1!01!I!#5.I97ZO:7QA-1!01!I!#5.I97ZO:7QA-A!01!I!#5.I97ZO:7QA-Q!01!I!#5.I97ZO:7QA.!"&lt;!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-41WBB&lt;GZF&lt;#V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!"!!!!!%!!A!$"U.I97ZO:7Q!!1!%!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Power Control-Cluster.ctl" Type="VI" URL="../Control/Power Control-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#:!!!!"!!/1#%)4X:F=H*J:'5!!!B!)1.4:81!&amp;E!B%%BJ:WAA5'^X:8)A1WRB=X-!!'5!]1!!!!!!!!!$&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=RF1&lt;X&gt;F=C"$&lt;WZU=G^M,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!!!!1!#$6"P&gt;W6S)%.P&lt;H2S&lt;WQ!!1!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Public Function" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Communication" Type="Folder">
			<Item Name="Open.vi" Type="VI" URL="../Method/Open.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%B!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!&amp;F"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!65(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
			</Item>
		</Item>
		<Item Name="Control Function" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Set Power.vi" Type="VI" URL="../Control Function/Set Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(2!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$E!B#%^W:8*S;72F!!!)1#%$5W6U!":!)2");7&gt;I)&amp;"P&gt;W6S)%.M98.T!!"F!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-:5'^X:8)A1W^O&gt;(*P&lt;#V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1V1&lt;X&gt;F=C"$&lt;WZU=G^M!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Set Tx Disable.vi" Type="VI" URL="../Control Function/Set Tx Disable.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%B!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!&amp;F"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%%!B#F2Y)%2J=W&amp;C&lt;'5!!%:!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!&amp;6"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Set Tx CDR.vi" Type="VI" URL="../Control Function/Set Tx CDR.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#%!B!U.%5A![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Set Rx CDR.vi" Type="VI" URL="../Control Function/Set Rx CDR.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#%!B!U.%5A![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Monitors" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Get Temperature.vi" Type="VI" URL="../Method/Get Temperature.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!,6'6N='6S982V=G5!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Voltage.vi" Type="VI" URL="../Method/Get Voltage.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(6G^M&gt;'&amp;H:1!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Tx Bias.vi" Type="VI" URL="../Method/Get Tx Bias.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(6(AA1GFB=Q!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Tx Power.vi" Type="VI" URL="../Method/Get Tx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)6(AA5'^X:8)!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Rx Power.vi" Type="VI" URL="../Method/Get Rx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)5HAA5'^X:8)!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get RSSI.vi" Type="VI" URL="../Method/Get RSSI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"A!%5F.431!!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Interrupt Flag" Type="Folder">
			<Item Name="Alarm Warning Flags" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Get Channel Alarm Warning Flag.vi" Type="VI" URL="../Alarm Warning Flags/Get Channel Alarm Warning Flag.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)6!!!!%!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%%!B#URP&gt;S"898*O;7ZH!"*!)1R);7&gt;I)&amp;&gt;B=GZJ&lt;G=!!!Z!)1F-&lt;X=A17RB=GU!%%!B#EBJ:WAA17RB=GU!!(9!]1!!!!!!!!!$&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9B&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=RF"&lt;'&amp;S&lt;3"898*O;7ZH,5.M&gt;8.U:8)O9X2M!#:!5!!%!!1!"1!'!!=417RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!%!!!!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!5!"F*F971A-A!!%E"!!!(`````!!Q%1HFU:1!!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!#!!*!!I!#1!*!!E!#1!,!!U!#1!/!Q!!?!!!$1A!!!E!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!##!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!]!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
				</Item>
				<Item Name="Get Temp Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Temp Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(P!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!"]!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!M1&amp;!!"!!&amp;!!9!"Q!)'&amp;2F&lt;8!A17RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!E!#A!%!!1!"!!%!!M!"!!%!!Q$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Voltage Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Voltage Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(R!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!"_!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!O1&amp;!!"!!&amp;!!9!"Q!)'V:P&lt;(2B:W5A17RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"5!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!U!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Tx Bias Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Tx Bias Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(R!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!"_!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!O1&amp;!!"!!&amp;!!9!"Q!)'V2Y)%*J98-A17RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"5!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!U!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Tx Power Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Tx Power Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(T!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!#!!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!Q1&amp;!!"!!&amp;!!9!"Q!)(&amp;2Y)&amp;"P&gt;W6S)%&amp;M98*N)&amp;&gt;B=GZJ&lt;G=A2GRB:X-!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Rx Power Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Rx Power Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(T!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!#!!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!Q1&amp;!!"!!&amp;!!9!"Q!)(&amp;*Y)&amp;"P&gt;W6S)%&amp;M98*N)&amp;&gt;B=GZJ&lt;G=A2GRB:X-!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
			</Item>
			<Item Name="Mask All Flags.vi" Type="VI" URL="../Method/Mask All Flags.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$E!B#%VB=WMA17RM!!![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Rx LOS Flag.vi" Type="VI" URL="../Method/Get Rx LOS Flag.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(@!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F$;'&amp;O&lt;G6M)$%!$E!B#5.I97ZO:7QA-A!/1#%*1WBB&lt;GZF&lt;#!T!!Z!)1F$;'&amp;O&lt;G6M)$1!:Q$R!!!!!!!!!!-64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T'U.I97ZO:7QA1G^P&lt;'6B&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!"!!&amp;!!9!"Q!)#V*Y)%R05S"'&lt;'&amp;H!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Tx LOS Flag.vi" Type="VI" URL="../Method/Get Tx LOS Flag.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(@!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F$;'&amp;O&lt;G6M)$%!$E!B#5.I97ZO:7QA-A!/1#%*1WBB&lt;GZF&lt;#!T!!Z!)1F$;'&amp;O&lt;G6M)$1!:Q$R!!!!!!!!!!-64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T'U.I97ZO:7QA1G^P&lt;'6B&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!"!!&amp;!!9!"Q!)#V2Y)%R05S"'&lt;'&amp;H!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!'%!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Tx Fault Flag.vi" Type="VI" URL="../Method/Get Tx Fault Flag.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(B!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F$;'&amp;O&lt;G6M)$%!$E!B#5.I97ZO:7QA-A!/1#%*1WBB&lt;GZF&lt;#!T!!Z!)1F$;'&amp;O&lt;G6M)$1!;1$R!!!!!!!!!!-64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T'U.I97ZO:7QA1G^P&lt;'6B&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!A1&amp;!!"!!&amp;!!9!"Q!)$62Y)%:B&gt;7RU)%:M97=!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!91$Q!!Q!!Q!%!!E!#A!%!!1!"!!%!!M!"!!%!!Q$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get IntL Flag.vi" Type="VI" URL="../Alarm Warning Flags/Get IntL Flag.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F*&lt;H2-)%:M97=!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
		</Item>
		<Item Name="Information" Type="Folder">
			<Item Name="Get Vendor Name.vi" Type="VI" URL="../A0/Get Vendor Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],6G6O:'^S)%ZB&lt;75!3%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!75(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"'1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!"61=G^E&gt;7.U)%VF&lt;7^S?3".98!A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Part Number.vi" Type="VI" URL="../A0/Get Part Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],5'&amp;S&gt;#"/&gt;7VC:8)!3%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!75(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"'1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!"61=G^E&gt;7.U)%VF&lt;7^S?3".98!A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Serial Number.vi" Type="VI" URL="../A0/Get Serial Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].5W6S;7&amp;M)%ZV&lt;7*F=A")1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!":1=G^E&gt;7.U)%VF&lt;7^S?3".98!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%:!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!&amp;6"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Set Serial Number.vi" Type="VI" URL="../A0/Set Serial Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%B!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!&amp;F"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!Q`````QV4:8*J97QA4H6N9G6S!%:!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!&amp;6"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!%+!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Length.vi" Type="VI" URL="../A0/Get Length.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!'4'6O:X2I!!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Threshold" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Default Threshold.vi" Type="VI" URL="../Threshold/Default Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Temp Threshold.vi" Type="VI" URL="../Threshold/Get Temp Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+=!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"O!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#*!5!!%!!E!#A!,!!Q/6'6N=#"5;(*F=WBP&lt;'1!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#6&amp;42F!S/#"J&lt;A"5!0!!$!!$!!A!$1!/!!]!$Q!0!!]!%!!0!!]!%1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!")!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Get Voltage Threshold.vi" Type="VI" URL="../Threshold/Get Voltage Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+?!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"Q!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#2!5!!%!!E!#A!,!!Q26G^M&gt;'&amp;H:3"5;(*F=WBP&lt;'1!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!#!!.!!Y!$Q!0!!]!$Q!1!!]!$Q!2!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!%A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Get Tx Bias Threshold.vi" Type="VI" URL="../Threshold/Get Tx Bias Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+?!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"Q!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#2!5!!%!!E!#A!,!!Q26(AA1GFB=S"5;(*F=WBP&lt;'1!0%"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!+56.'5$)Y)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!#!!.!!Y!$Q!0!!]!$Q!1!!]!$Q!2!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!%A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Get Tx Power Threshold.vi" Type="VI" URL="../Threshold/Get Tx Power Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+A!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"S!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#:!5!!%!!E!#A!,!!Q36(AA5'^X:8)A6'BS:8.I&lt;WRE!!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!)!!U!$A!0!!]!$Q!0!"!!$Q!0!"%$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!3!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
			<Item Name="Get Rx Power Threshold.vi" Type="VI" URL="../Threshold/Get Rx Power Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+A!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"S!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#:!5!!%!!E!#A!,!!Q35HAA5'^X:8)A6'BS:8.I&lt;WRE!!!]1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!J25U:1-DAA&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!*B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)/56.'5$)Y,GRW9WRB=X-!!!F25U:1-DAA;7Y!6!$Q!!Q!!Q!)!!U!$A!0!!]!$Q!0!"!!$Q!0!"%$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!3!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="Calibration" Type="Folder">
			<Item Name="Default Calibration.vi" Type="VI" URL="../Calibration/Default Calibration.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!G&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AZ25U:1-DAO&lt;(:D&lt;'&amp;T=Q!!#F&amp;42F!S/#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/E"Q!"Y!!#964X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$F&amp;42F!S/#ZM&gt;G.M98.T!!!*56.'5$)Y)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="QSFP28.vi" Type="VI" URL="../QSFP28.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
	</Item>
</LVClass>
