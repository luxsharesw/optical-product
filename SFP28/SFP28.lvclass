﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str"></Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Optical Product.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Optical Product.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*.!!!*Q(C=\&gt;1R4O.1%-&lt;R,YBC3X)"B(+&amp;O5'5+_1!;;;FW#,&amp;.GSTO%")F#.RAFQB6ZALZ!KZAP@PZ]EGCY!U)&amp;&amp;A]YTTP@@G`7)\FNLW1\J5@W[\@8=&lt;_Y&gt;B@2X'4]??Y8"\=KD`&gt;89=^8,KS@T$B&amp;=KV&gt;$_P[YK&gt;4L`&lt;@_Z4?^OX`X@`6_\``TD@8@_2`#ZGT2J5FL3AO9U;^_O4`)E4`)E4`)E$`)A$`)A$`)A&gt;X)H&gt;X)H&gt;X)H.X)D.X)D.X)D&lt;TOZS%5O=KZ+MHCS5&amp;)U+:!-BK,EE(A34_**0(R5YEE]C3@R*"['+0%EHM34?")0UZ2Y%E`C34S*BV*.EGUHRZ.Y++`!%XA#4_!*0#SJQ"-!AM7#QE%2'!I[AZ0!%XA#$[=+0)%H]!3?Q%/X!E`A#4S"*`!QJ6W6;*JB*]&gt;$'4E?R_.Y()`DI&lt;1=D_.R0)\(]&lt;#=()`D=2$/AEZR#()G/1/=$Y\(]@"0DM@R/"\(YXDI;H@)WZ5:.-./DM@Q'"\$9XA-$S6E?!S0Y4%]BI?S-DS'R`!9(M0$5D)]BM@Q'""D5:;85=S9;!QS!M0$8XN;L.WF;"*L?`UURQ&gt;6^1#K(CT6![.[%&amp;1X7(8D6$&gt;%&gt;;&amp;6&amp;V"V962@706&amp;6%$6QKK#KI(;=^T2NL1.L;/N;5P;AD;D49?J(TRQP^^LN^NJO^VKM^GI[TKNVWMNFUMN&amp;AP.:D..J^.`&lt;[M&lt;^H'&lt;(.Z,&gt;ZT@`XJ?&gt;@0ZUZ_@$[P@][OHR`HV;MA0`:`2RP@36XAX[E,^S7O?;`18C&lt;'70A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.3</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&lt;O5F.31QU+!!.-6E.$4%*76Q!!%NQ!!!13!!!!)!!!%LQ!!!!I!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!MF/^/&amp;$,U%W6OS@US:]%-1!!!!Q!!!!1!!!!!+0&gt;EB&lt;?WMZ(LZ\IV%$E'`45(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#Y'1+K0B:-2)6.PT"\;5EB!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%#_0"L\T-XDRA1E^#`SDH[I!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!I!!!!,(C=9_"G9'JAO-!!R)R!T.4!"71T`1$S0Y$%'!1Y%#1!%%U-:1!!!%A!!!%9?*RD9-!%`Y%!3$%S-$!T!7F7.(%Q$7.4%_!S&amp;Z&gt;&gt;5(&amp;G)':"O*-2+-;U"]A!O:U&amp;KE9+)M@U!IB0I*P$BM6M!-+3*YQ!!!!-!!&amp;73524!!!!!!!$!!!##1!!!\RYH!NA9G$).,9Q7Q#EG2E:'-1:'BC3]V.3O2C!@!9)7!*D5!!#I/:JI9E&lt;(DC="A2[`0)N9(\T'ZZO&amp;R7"ZBI6#;:3E7Y@&amp;:&amp;/(R774B;6&amp;X`_```@@)4H=,&gt;(TH&amp;('Z$;&lt;A[A_(%8&amp;1Y1"UCTA/D`A2EA64$T!I#G=425+$/5M"A?C$J]P-'%%7)RT-AI60N$/Z]'BY&gt;VBQJU_X*U4P1$/LE\6K2T:B#1]9F@A!G)O&gt;0Y^5Z7MQ(*!V5+;@Q;&amp;T\R(\L)E-:@&gt;[(Z!3.1^#"`[TKA1@R&gt;+Y"E&lt;R&amp;(ZU;1+&lt;X6(*U\I;9Y=1!*&lt;QYE=S3QG6/;6:82'Q:V27]]"^Q6&lt;C$^`I4V;V6J^$JR&gt;$;#^4M$'3$^;&gt;V/('H7TBT]5Q\Q&lt;TOR!R1;`+Y(Y_+D9Y&amp;BZA!-@QZAI!M=@-D3X;A"]M"%%!E5YOE-Y:!Y\M+B)Q&lt;E-Z\I"!:X*Q]M6DDAY2U'-K"%2;$4""1T,#$DQ7K[W9Y\;)$DS5%%1G6!K!I)61#C&gt;I"4Q"'/O-0Q^,(W^&lt;V&gt;I(4$BJ2G()#Y!9B";29:[T%Q-I!M:!+3N6#V.E!W%V1-FP:!\!01:+#"J/=*)]*]E"[1T"SI')C^#-JOA,I(*097+$9"SPY#:#&gt;!W&lt;^"YIQ1.B01-!'I/#?1@1%KTA&gt;FA^T/Q)#&lt;&gt;P:X=58/-\$]#!#&gt;O--Q!!!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"Y?!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"YL+KLL(A!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!"YL+KDI[/DK[RY!!!!!!!!!!!!!!!!!!!!!0``!!"YL+KDI[/DI[/DI[OM?!!!!!!!!!!!!!!!!!!!``]!K[KDI[/DI[/DI[/DI[/LL!!!!!!!!!!!!!!!!!$``Q#KKK/DI[/DI[/DI[/DI`[L!!!!!!!!!!!!!!!!!0``!+KLK[KDI[/DI[/DI`\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OKI[/DI`\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLKKT_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#LK[OLK[OLK`\_`P\_`KOL!!!!!!!!!!!!!!!!!0``!!#EKKOLK[OL`P\_`KOMJ!!!!!!!!!!!!!!!!!!!``]!!!!!J+OLK[P_`KOLJ!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!+3LK[OLIQ!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!#EIQ!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!"'M!!""@?*T.FV^I5V==RX`H^L&lt;?N*'?&gt;(9;K-N.&gt;B.&amp;&amp;FNEU[\-T;;XX3IC&lt;&gt;-[X:#;.N%*OI[GX1I$O\'\&lt;HXQS&gt;("5.B4]5(91R\'I)+/U*@\I(M26A?:@&gt;HD@*B9/G`O@O@=X.T];&gt;/O+%U?@BRO@N`@XQ]HO1$U1\J(S-)6!QB^AI=4"N4(&gt;1+1$EO1_Y2GA![2@Y(M]B)$DEF$^+'1*3U'O/.[3'J4:_%R?JPXT-`*.;'&gt;0E,8/OL&amp;905'.-&lt;VX:\D3I9K0\5IM\6W6!`Y[&amp;73&amp;5YLPK@367U-%Y+WHVF0G'3"K(Z2V!*^M9]4GM+?OM+3FY&gt;U'5"6@??YENG(%4(VLTSE=)OEB.NW3-#1_S'63DECDS5+]4,;55.3!#1NX+KA;6,VP5EF]RL8V(-.ZEH&lt;?62@^BKLH9F+J3_J/E5J[K:S,4/NL4//=&gt;XS]D,KU/:U(RGQ3]F%*+`U+,Z5O\.@_RE)VDAMG&gt;_:1UTP/=GWQ(W&lt;="("$E+D?)Y;=%$4B8-AWO/^#^@Z'E2\$7`B'EA08Y.,H;8A\/(["HN1R82A&lt;^?FS?2%9FQ?/S_08IIFE`)HYR=`D5UEZ(BM)F;_I;/K\DL#OG@*/"T1$#,=BJ\#;9`"`0Q]$A#N)XU&lt;J&lt;O64&amp;\HY&gt;U=TU]]\ES=:85G^QZ/4FU:P-#G&amp;_Q1&lt;':&amp;TGQLN`X=HO8W3Y@C1UCRBZ977E4R']_@YM-)VZ53CC%#.S&amp;;A=ADFMCB'0U"@I&amp;)"5U\;K9,+5:."$82D3F_MYRCJCWB?'ZOLEC(O_D)5SQ39F'MX4?@G=]9SUPG0`!!4H'7,4\\="VRP1YC,-FF40)^&gt;0,Z\\$H`^7;_$&lt;A!P:68I!M!%.9#QR%I\UF3`D4!#WOC[Z$3K;_C.2'#-&amp;2OUP4Q\N=86X&amp;,N&amp;;A45`@#%+GM+?O-,;20"AW`F=7,="8S0%BZ7-Q+/W]7IJ:GY".U:;7&amp;D!3'A^0GR2I&amp;[3L:(40GO]&lt;H32O9O@72L)L7B5#L$QZ;ZM_HZGCVV&gt;0K?;';Q'?[QJK/96V"`9?D5DG[^GJ+C;"A/_585P]N&amp;I&lt;@+/-`&amp;GL/&lt;XJ4_QGMG:(XEVN3Q&amp;C\,3:K8DZ^;#]]'#=^AZ0`WN)#&gt;S`'X:J&gt;+!\8&gt;O@+H-)MD"-R$]!.36A1&lt;L;KE*T1AB'5*_-R3Q8:(AMXG#OT$%Z6+#B^=F_06.%:Q/\"C=EC-89]FSCM]^.YJ@(&lt;\A="/L+IJ(KILCU7WA/,Z6CB.L5#QCR9OS:$[W`O)N_K6J_RD)``&amp;T[/\0U_X&amp;ZVXY)V"%&gt;X2&gt;OC-&lt;U&amp;W8IVM;G*,\RDZ,D*@D0@BC]"[K+LR0628?\W]$XK?XCP?:.@#O`8^YN^JYGX`D=W`JZ&gt;WV,N[&gt;G\S]J=&amp;V]6:@$.\&gt;696X4V8B`?YWY0X?6P(O810POMXB\4J:]&amp;YA06S=&gt;,[,+KJ:S9&amp;W9`:J!ZL6LK:\N"=HY::[J7Z[!^_:]08J:?G'^#4^)0`3HT[26WLXY3_B5@H"*_XZ$_L-C11!!!!!"!!!!%A!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!#]A!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!&gt;B=!A!!!!!!"!!A!-0````]!!1!!!!!!7A!!!!5!$U!+!!B5?#"1&lt;X&gt;F=A!!$U!+!!B3?#"1&lt;X&gt;F=A!!$5!+!!&gt;5?#"#;7&amp;T!!N!"A!%5F.431!!(%"1!!1!!!!"!!)!!QV42F!S/#ZM&gt;G.M98.T!!%!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!V&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!%!!!!!!!!!!%!!!!#!!!!!Q!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VR&lt;4]1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$8&amp;N0R!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!(98!)!!!!!!!1!)!$$`````!!%!!!!!!&amp;I!!!!&amp;!!^!#A!)6(AA5'^X:8)!!!^!#A!)5HAA5'^X:8)!!!V!#A!(6(AA1GFB=Q!,1!9!"&amp;*45UE!!"R!5!!%!!!!!1!#!!-.5U:1-DAO&lt;(:D&lt;'&amp;T=Q!"!!1!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!"I!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!@"=!A!!!!!!&amp;!!^!#A!)6(AA5'^X:8)!!!^!#A!)5HAA5'^X:8)!!!V!#A!(6(AA1GFB=Q!,1!9!"&amp;*45UE!!"R!5!!%!!!!!1!#!!-.5U:1-DAO&lt;(:D&lt;'&amp;T=Q!"!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"A!.!!!!"!!!!)E!!!!I!!!!!A!!"!!!!!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!3=!!!(;?*S64]V+QU!9H(&lt;4J%HND^87+AD\!#+C&amp;Y^"I?$*E0A!RPR)9#%FW&gt;1?_YY?@!X\"E[3KA&gt;0-P$NT0=\#_!-9^&gt;"`WEDP@QN+&gt;"+`U=/+3V7\\+QR-!V9@B"]!#EW(X-XX=!R/RRJ&lt;-I6.)L]LC+^+6;K_RF'#S^[VPS3)6F;&lt;=KUAJ8LA?$=RVU)=\P668KJ*"Z+JN'O3KS&gt;;A4'9=[2)_!AZD&gt;HYU1?YE&lt;7,"&gt;#S*6L_C\7Z(\OEUE?9I*&lt;QD9\"W)+EYZ[)$""+&lt;.^7'$E&gt;#&lt;G"704AYQ*IOYW;3TCX^Z\.;\/U!@TZQRM/2'C3/-'MM^(.0L.XJ\`-X]6G9.WH^W-.`\-MBL4V.'E\=G/#1`Q9+Z"8^\3D8FW[WN@!'LH5&lt;)!!!!!)Q!!1!#!!-!"!!!!%A!$Q!!!!!!$Q$N!/-!!!"?!!]!!!!!!!]!\1$D!!!!&gt;!!0!!!!!!!0!/U!YQ!!!)K!!)!!A!!!$Q$N!/-647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E"-&amp;*45E-.#A!$4&amp;:$1UR#6F=!!",=!!!%%A!!!#!!!"+]!!!!!!!!!!!!!!!A!!!!.!!!"!A!!!!&lt;4%F#4A!!!!!!!!&amp;54&amp;:45A!!!!!!!!&amp;I5F242Q!!!!!!!!&amp;]1U.46!!!!!!!!!'14%FW;1!!!!!!!!'E1U^/5!!!!!!!!!'Y6%UY-!!!!!!!!!(-2%:%5Q!!!!!!!!(A4%FE=Q!!!!!!!!(U6EF$2!!!!!!!!!))&gt;G6S=Q!!!!1!!!)=5U.45A!!!!!!!!+!2U.15A!!!!!!!!+535.04A!!!!!!!!+I;7.M/!!!!!!!!!+]4%FG=!!!!!!!!!,12F")9A!!!!!!!!,E2F"421!!!!!!!!,Y6F"%5!!!!!!!!!--4%FC:!!!!!!!!!-A1E2)9A!!!!!!!!-U1E2421!!!!!!!!.)6EF55Q!!!!!!!!.=2&amp;2)5!!!!!!!!!.Q466*2!!!!!!!!!/%3%F46!!!!!!!!!/96E.55!!!!!!!!!/M2F2"1A!!!!!!!!0!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!M!!!!!!!!!!$`````!!!!!!!!!.!!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!$M!!!!!!!!!!$`````!!!!!!!!!2A!!!!!!!!!!0````]!!!!!!!!")!!!!!!!!!!!`````Q!!!!!!!!&amp;-!!!!!!!!!!$`````!!!!!!!!!:A!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!%`````Q!!!!!!!!/Y!!!!!!!!!!@`````!!!!!!!!!]A!!!!!!!!!#0````]!!!!!!!!$W!!!!!!!!!!*`````Q!!!!!!!!0I!!!!!!!!!!L`````!!!!!!!!!`A!!!!!!!!!!0````]!!!!!!!!%#!!!!!!!!!!!`````Q!!!!!!!!1A!!!!!!!!!!$`````!!!!!!!!"$1!!!!!!!!!!0````]!!!!!!!!%O!!!!!!!!!!!`````Q!!!!!!!!C]!!!!!!!!!!$`````!!!!!!!!#-Q!!!!!!!!!!0````]!!!!!!!!.0!!!!!!!!!!!`````Q!!!!!!!!V%!!!!!!!!!!$`````!!!!!!!!$5Q!!!!!!!!!!0````]!!!!!!!!.8!!!!!!!!!!!`````Q!!!!!!!!X%!!!!!!!!!!$`````!!!!!!!!$=Q!!!!!!!!!!0````]!!!!!!!!1R!!!!!!!!!!!`````Q!!!!!!!"$-!!!!!!!!!!$`````!!!!!!!!%.1!!!!!!!!!!0````]!!!!!!!!2!!!!!!!!!!#!`````Q!!!!!!!")M!!!!!!F42F!S/#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!1!!1!!!!!!!!!!!!!"!"2!5!!!$6.'5$)Y,GRW9WRB=X-!!1!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!"``]!!!!"!!!!!!!"!!!!!!%!&amp;%"1!!!.5U:1-DAO&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!)64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!1!!!!!!!A!!!!!(!!^!#A!)6(AA5'^X:8)!!!^!#A!)5HAA5'^X:8)!!!V!#A!(6(AA1GFB=Q!21!I!#V2F&lt;8"F=G&amp;U&gt;8*F!!V!#A!(6G^M&gt;'&amp;H:1!,1!9!"&amp;*45UE!!'I!]&gt;=7UBI!!!!$&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T#6.'5$)Y,G.U&lt;!!U1&amp;!!"A!!!!%!!A!$!!1!"2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!9!!!!'````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!"!!!!!!!$!!!!!!5!$U!+!!B5?#"1&lt;X&gt;F=A!!$U!+!!B3?#"1&lt;X&gt;F=A!!$5!+!!&gt;5?#"#;7&amp;T!!N!"A!%5F.431!!:A$RVR&lt;4]1!!!!-64X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-*5U:1-DAO9X2M!$"!5!!%!!!!!1!#!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!"!!!!!!!!!!"!!!!!A!!!!5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9B&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"I!!!!!B60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X.16%AQ!!!!,A!"!!1!!!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!</Property>
	<Item Name="SFP28.ctl" Type="Class Private Data" URL="SFP28.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="RSSI" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:RSSI</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">RSSI</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
			<Item Name="Read RSSI.vi" Type="VI" URL="../Accessor/RSSI Property/Read RSSI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"A!%5F.431!!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write RSSI.vi" Type="VI" URL="../Accessor/RSSI Property/Write RSSI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!9!"&amp;*45UE!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Rx Power" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Rx Power</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Rx Power</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
			<Item Name="Read Rx Power.vi" Type="VI" URL="../Accessor/Rx Power Property/Read Rx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)5HAA5'^X:8)!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Rx Power.vi" Type="VI" URL="../Accessor/Rx Power Property/Write Rx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!01!I!#&amp;*Y)&amp;"P&gt;W6S!!!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Tx Bias" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Bias</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Bias</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
			<Item Name="Read Tx Bias.vi" Type="VI" URL="../Accessor/Tx Bias Property/Read Tx Bias.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(6(AA1GFB=Q!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Tx Bias.vi" Type="VI" URL="../Accessor/Tx Bias Property/Write Tx Bias.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!I!"V2Y)%*J98-!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Tx Power" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Monitor:Tx Power</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tx Power</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
			<Item Name="Read Tx Power.vi" Type="VI" URL="../Accessor/Tx Power Property/Read Tx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)6(AA5'^X:8)!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Tx Power.vi" Type="VI" URL="../Accessor/Tx Power Property/Write Tx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!01!I!#&amp;2Y)&amp;"P&gt;W6S!!!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Public Function" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Communication" Type="Folder">
			<Item Name="Open.vi" Type="VI" URL="../Method/Open.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!":1=G^E&gt;7.U)%VF&lt;7^S?3".98!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%2!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!"61=G^E&gt;7.U)%VF&lt;7^S?3".98!A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082929680</Property>
			</Item>
		</Item>
		<Item Name="Control Function" Type="Folder">
			<Item Name="Set Tx Disable.vi" Type="VI" URL="../Control Function/Set Tx Disable.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!":1=G^E&gt;7.U)%VF&lt;7^S?3".98!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!)1J5?#"%;8.B9GRF!!"%1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!65(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Monitors" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Get Temperature.vi" Type="VI" URL="../Monitors/Get Temperature.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!,6'6N='6S982V=G5!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Voltage.vi" Type="VI" URL="../Monitors/Get Voltage.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%[!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(6G^M&gt;'&amp;H:1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Tx Bias.vi" Type="VI" URL="../Monitors/Get Tx Bias.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%[!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(6(AA1GFB=Q!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Tx Power.vi" Type="VI" URL="../Monitors/Get Tx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)6(AA5'^X:8)!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Rx Power.vi" Type="VI" URL="../Monitors/Get Rx Power.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)5HAA5'^X:8)!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get RSSI.vi" Type="VI" URL="../Monitors/Get RSSI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"A!%5F.431!!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Interrupt Flag" Type="Folder">
			<Item Name="Alarm Warning Flags" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Get Temp Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Temp Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(J!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!"]!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!M1&amp;!!"!!&amp;!!9!"Q!)'&amp;2F&lt;8!A17RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!6!$Q!!Q!!Q!%!!E!#A!%!!1!"!!%!!M!"!!%!!Q$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Voltage Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Voltage Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(L!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!"_!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!O1&amp;!!"!!&amp;!!9!"Q!)'V:P&lt;(2B:W5A17RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"5!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!U!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Tx Bias Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Tx Bias Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(L!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!"_!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!O1&amp;!!"!!&amp;!!9!"Q!)'V2Y)%*J98-A17RB=GUA6W&amp;S&lt;GFO:S"'&lt;'&amp;H=Q!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"5!0!!$!!$!!1!#1!+!!1!"!!%!!1!#Q!%!!1!$!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!U!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Tx Power Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Tx Power Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(N!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!#!!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!Q1&amp;!!"!!&amp;!!9!"Q!)(&amp;2Y)&amp;"P&gt;W6S)%&amp;M98*N)&amp;&gt;B=GZJ&lt;G=A2GRB:X-!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
				<Item Name="Get Rx Power Alarm Warning.vi" Type="VI" URL="../Alarm Warning Flags/Get Rx Power Alarm Warning.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(N!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N-&lt;X=A6W&amp;S&lt;GFO:Q!31#%-3'FH;#"898*O;7ZH!!!/1#%*4'^X)%&amp;M98*N!""!)1J);7&gt;I)%&amp;M98*N!!#!!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-:17RB=GUA6W&amp;S&lt;GFO:SV$&lt;(6T&gt;'6S,G.U&lt;!!Q1&amp;!!"!!&amp;!!9!"Q!)(&amp;*Y)&amp;"P&gt;W6S)%&amp;M98*N)&amp;&gt;B=GZJ&lt;G=A2GRB:X-!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
				</Item>
			</Item>
			<Item Name="Get IntL Flag.vi" Type="VI" URL="../Interrupt Flag/Get IntL Flag.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F*&lt;H2-)%:M97=!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972416</Property>
			</Item>
			<Item Name="Get Rx LOS Flag.vi" Type="VI" URL="../Interrupt Flag/Get Rx LOS Flag.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:3?#"-4V-!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
		</Item>
		<Item Name="Information" Type="Folder">
			<Item Name="Set Serial Number.vi" Type="VI" URL="../A0/Set Serial Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!":1=G^E&gt;7.U)%VF&lt;7^S?3".98!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````].5W6S;7&amp;M)%ZV&lt;7*F=A"%1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!65(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!1I!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Vendor Name.vi" Type="VI" URL="../A0/Get Vendor Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],6G6O:'^S)%ZB&lt;75!2E"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!&amp;F"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!&amp;6"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Part Number.vi" Type="VI" URL="../A0/Get Part Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],5'&amp;S&gt;#"/&gt;7VC:8)!2E"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!&amp;F"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!&amp;6"S&lt;W2V9X1A476N&lt;X*Z)%VB=#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Serial Number.vi" Type="VI" URL="../A0/Get Serial Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].5W6S;7&amp;M)%ZV&lt;7*F=A"'1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!75(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"%1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!65(*P:(6D&gt;#".:7VP=HEA47&amp;Q)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Length.vi" Type="VI" URL="../A0/Get Length.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%[!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!'4'6O:X2I!!!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Threshold" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Default Threshold.vi" Type="VI" URL="../Threshold/Default Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Get Temp Threshold.vi" Type="VI" URL="../Threshold/Get Temp Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+7!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"O!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#*!5!!%!!E!#A!,!!Q/6'6N=#"5;(*F=WBP&lt;'1!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#&amp;.'5$)Y)'FO!!"5!0!!$!!$!!A!$1!/!!]!$Q!0!!]!%!!0!!]!%1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!")!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Get Voltage Threshold.vi" Type="VI" URL="../Threshold/Get Voltage Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+9!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"Q!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#2!5!!%!!E!#A!,!!Q26G^M&gt;'&amp;H:3"5;(*F=WBP&lt;'1!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!#!!.!!Y!$Q!0!!]!$Q!1!!]!$Q!2!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!%A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Get Tx Bias Threshold.vi" Type="VI" URL="../Threshold/Get Tx Bias Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+9!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"Q!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#2!5!!%!!E!#A!,!!Q26(AA1GFB=S"5;(*F=WBP&lt;'1!/%"Q!"Y!!#564X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC$6.'5$)Y,GRW9WRB=X-!#6.'5$)Y)'^V&gt;!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!#!!.!!Y!$Q!0!!]!$Q!1!!]!$Q!2!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!%A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Get Tx Power Threshold.vi" Type="VI" URL="../Threshold/Get Tx Power Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+;!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"S!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#:!5!!%!!E!#A!,!!Q36(AA5'^X:8)A6'BS:8.I&lt;WRE!!!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!6!$Q!!Q!!Q!)!!U!$A!0!!]!$Q!0!"!!$Q!0!"%$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!3!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
			<Item Name="Get Rx Power Threshold.vi" Type="VI" URL="../Threshold/Get Rx Power Threshold.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+;!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!'!!J);7&gt;I)%&amp;M98*N!!!01!9!#5RP&gt;S""&lt;'&amp;S&lt;1!41!9!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!'!!N-&lt;X=A6W&amp;S&lt;GFO:Q"T!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-;6'BS:8.I&lt;WRE)%*Z&gt;'5N1WRV=X2F=CZD&gt;'Q!)E"1!!1!"!!&amp;!!9!"QZ5;(*F=WBP&lt;'1A1HFU:1!!%5!+!!J);7&gt;I)%&amp;M98*N!!!01!I!#5RP&gt;S""&lt;'&amp;S&lt;1!41!I!$%BJ:WAA6W&amp;S&lt;GFO:Q!!%5!+!!N-&lt;X=A6W&amp;S&lt;GFO:Q"S!0%!!!!!!!!!!R60=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7)84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-66'BS:8.I&lt;WRE,5.M&gt;8.U:8)O9X2M!#:!5!!%!!E!#A!,!!Q35HAA5'^X:8)A6'BS:8.I&lt;WRE!!!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!*5U:1-DAA&lt;X6U!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!B42F!S/#"J&lt;A!!6!$Q!!Q!!Q!)!!U!$A!0!!]!$Q!0!"!!$Q!0!"%$!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!3!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
			</Item>
		</Item>
		<Item Name="Calibration" Type="Folder">
			<Item Name="Default Calibration.vi" Type="VI" URL="../Calibration/Default Calibration.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9AV42F!S/#ZM&gt;G.M98.T!!F42F!S/#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*260=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7).5U:1-DAO&lt;(:D&lt;'&amp;T=Q!)5U:1-DAA;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="SFP28.vi" Type="VI" URL="../SFP28.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
	</Item>
</LVClass>
